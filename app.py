from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!, This is update N  via  CI/CD, intended for Jenkins and Playbook Clusters via a flask app'
